provider "aws" {
  region = "eu-west-1"
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

module "vpc" {
  source = "./vpc"

  name = "simple-example"

  cidr = "10.0.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b", "euw1-az3"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_ipv6 = false

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = {
    Name = "name-public"
  }

  tags = {
    Owner       = "user"
    Environment = "elastic"
  }

  vpc_tags = {
    Name = "vpc-name"
  }
}


module "sg" {
  source        = "./sg"
  vpc_id        = module.vpc.vpc_id
  ssh_ip        = ["0.0.0.0/0"]
  name   = "test"

  tags = {
    Owner       = "env"
    Environment = "elastic"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    values = [
      "ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"
    ]
  }
  filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }
  owners = ["099720109477"]
}

resource "aws_eip" "this" {
  vpc      = true
  instance = module.ec2.id[0]
}

resource "aws_placement_group" "elastic" {
  name     = "elastic-dev"
  strategy = "cluster"
}

resource "aws_kms_key" "this" {
}

# resource "tls_private_key" "example" {
#   algorithm = "RSA"
#   rsa_bits  = 4096
# }


variable "key_path" {
  default = "/Users/ankit/.ssh/id_rsa.pub"
}

variable "ssh_key_private" {
  default = "/Users/ankit/.ssh/id_rsa"
}



resource "aws_key_pair" "generated_key" {
  key_name   = "elasticsearchtest"
  public_key = file(var.key_path)
}

module "ec2" {
  source = "./ec2"
  instance_count = 1
  name          = "estest"
  ami           = data.aws_ami.ubuntu.id
  instance_type = "c5.large"
  key_name = aws_key_pair.generated_key.key_name
  subnet_id     = tolist(module.vpc.public_subnets)[0]
  vpc_security_group_ids      = [module.sg.remote_sg_id,module.sg.public_sg_id]
  associate_public_ip_address = true
  # cpu_credits   = "unlimited"
  placement_group             = aws_placement_group.elastic.id
  ssh_key_private = var.ssh_key_private

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 20
    },
  ]
  tags = {
    "Env"      = "Private"
    "Location" = "Secret"
  }

}