variable name {}
variable ssh_ip {
    type = list(string)
}
variable vpc_id {}

variable "tags" {
    type = map(string)
}


resource "aws_security_group" "public" {
  name        = "${var.name}-public-sg"
  vpc_id      = var.vpc_id

  tags = merge(
    var.tags,
    {
      "Name" = format("%s", var.name)
    },
  )

  lifecycle {
    create_before_destroy = true
  }
  # HTTP access from anywhere
  ingress {
  from_port   = 9200
  to_port     = 9200
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
 # outbound internet access
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

# Security group to access the instances over SSH and RDP
resource "aws_security_group" "remote" {
  name        = "${var.name}-remote-sg"
  vpc_id      = var.vpc_id
  tags = merge(
    var.tags,
    {
      "Name" = format("%s", var.name)
    },
  )

  lifecycle {
    create_before_destroy = true
  }
  ingress {
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = var.ssh_ip
  }

 # outbound internet access
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}