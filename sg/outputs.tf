output "remote_sg_id" {
    value = "${element(concat(aws_security_group.remote.*.id, list("")), 0)}"
}
output "public_sg_id" {
    value = "${element(concat(aws_security_group.public.*.id, list("")), 0)}"
}