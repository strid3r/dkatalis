* execute - **ansible-galaxy install elastic.elasticsearch,7.6.1**

* version :

  - ansible - ansible 2.9.6

  - terraform - 0.12.24

* In main.tf change the variable value **key_path** and **ssh_key_private** to appropriate value according to your workstation .

* TO enable the TLS in elasticsearch generate a SSL key and change the variable values in the playbook dkatalis/ec2/elasticsearchdeploy.yml

* Generate a self signed SSL key for testing purpose
```
openssl req -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out example.crt -keyout example.key
```
 * Execute the ```terraform apply``` in the cloned directory .
* To test connect with **https://[username]:[password]@[host]:9200/**

*******************************************************************************************************************

# Q&A

a) A short description of your solution describing your choices and why did you make them:

> I have used **terraform** for bootstrapping the ec2 instance as it's very easy to build, change and versioning of infrastructure safely and efficiently . It provides a flexible abstraction of resources and multiple cloud providers. It helps in organising and writing the infrastructure code into modules so interacting with a component does not affect the other components .
Then I used **ansible** for provision of elasticsearch in the launched ec2 instance instead of using a baked ami as changing and maintaining a setup is too tedious when dealing with a baked ami . With ansible we can patch , manage and extend the configs any time . 

b) Resources, if any, that you consulted to arrive at the final solution:

```
https://github.com/elastic/ansible-elasticsearch
```

c)How would you monitor this instance? What metrics would you monitor?
  > Using Cloudwatch agents we can monitor the disk utilization ,disk I/O , CPU utilization , memory utilization and status check of the running instances and notify the user if anyone of the following metrics at the  instance level cross threshold value . For load average , network we can work with other monitoring tools like prometheus  with graffand ,zabbix to create deep understanding of instances usage metrics . 
	  >  Then working with elasticsearch we have to consider few things to monitor like 
		  * Cluster Health
		  * Request Latency
		  * Refresh Times
		  * Indexing Performance
  > 
  d .) Could you extend your solution to replace a running ElasticSearch instance with little or no
downtime? How? 

e.) Could you extend your solution to launch a secure cluster of ElasticSearch nodes? What
would need to change to support this use case?

  >  Launch each node in a private subnet for security purpose with master behind a load balancer in different azs to achieve redundancy and HA with each az running a data node .
     Use ansible-vault for storing the secrets .

f) 6. Was it a priority to make your code well structured, extensible, and reusable?
 > yes
